package ru.spb.beavers.core.gui;

import ru.spb.beavers.core.components.FixedJPanel;
import ru.spb.beavers.core.data.StorageModules;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * View входных параметров модуля
 */
public class InputView extends JPanel {

    public final JPanel inputPanel = new FixedJPanel();
    private final JButton btnGoDescription = new JButton("Описание");
    private final JButton btnExecute = new JButton("Решить");
    private final JButton btnLoad = new JButton("Загрузить");
    private final JButton btnSave = new JButton("Сохранить");

    private final InputViewPresenter presenter = new InputViewPresenter(this);

    public InputView() {
        super(null);
        btnGoDescription.setSize(100, 30);
        btnGoDescription.setLocation(20,450);
        this.add(btnGoDescription);

        inputPanel.setBorder(BorderFactory.createLineBorder(Color.black));
        inputPanel.setSize(850, 400);
        inputPanel.setLayout(null);

        JScrollPane inputPanelWrapper = new JScrollPane(inputPanel);
        inputPanelWrapper.setSize(850, 400);
        inputPanelWrapper.setLocation(20, 20);
        this.add(inputPanelWrapper);

        btnSave.setSize(100, 30);
        btnSave.setLocation(530, 450);
        this.add(btnSave);

        btnLoad.setSize(100, 30);
        btnLoad.setLocation(650, 450);
        this.add(btnLoad);

        btnExecute.setSize(100, 30);
        btnExecute.setLocation(770, 450);
        this.add(btnExecute);

        initListeners();
    }

    private void initListeners() {
        btnGoDescription.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                presenter.btnGoDescriptionPressed();
            }
        });

        btnExecute.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                presenter.btnExecutePressed();
            }
        });
    }

    public void initSaveButtonListener(ActionListener listener) {
        presenter.initSaveButtonListener(listener);
    }

    public void initLoadButtonListener(ActionListener listener) {
        presenter.initLoadButtonListener(listener);
    }

    public class InputViewPresenter {

        private InputView view;

        public InputViewPresenter(InputView view) {
            this.view = view;
        }

        public void btnGoDescriptionPressed() {
             GUIManager.setActiveView(GUIManager.getDescriptionView());
        }

        public void btnExecutePressed() {
            ExampleView exampleView = GUIManager.getExampleView();
            StorageModules.getActiveModule().initExamplePanel(exampleView.getExamplePanel());
            GUIManager.setActiveView(exampleView);
        }

        public void initSaveButtonListener(final ActionListener listener) {
            view.btnSave.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent event) {
                    try {
                        listener.actionPerformed(event);
                    } catch (IllegalArgumentException ex) {
                        JOptionPane.showMessageDialog(view, "Не удалось сохранить",
                                "Ошибка", JOptionPane.ERROR_MESSAGE);
                    }
                }
            });
        }

        public void initLoadButtonListener(final ActionListener listener) {
            view.btnLoad.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent event) {
                    try {
                        listener.actionPerformed(event);
                    } catch (IllegalArgumentException ex) {
                        JOptionPane.showMessageDialog(view, "Не удалось загрузить",
                                "Ошибка", JOptionPane.ERROR_MESSAGE);
                    }
                }
            });
        }
    }

}
